//init all dependencies
const express = require('express');
const env = require('dotenv').config();
const cors = require('cors');
const helmet = require('helmet');
const path = require('path');

// const multer = require('multer');
// const upload = multer();

//init listen port
const PORT = 8080;

const app = express();
app.use(cors());
app.use(helmet());




// app.use(bodyParser.urlencoded({extended: true, limit: '50mb', parameterLimit: 1000000}));
// app.use(bodyParser.json({limit: '50mb'}));
// Add headers
// app.use(function (req, res, next) {
//
//   // Website you wish to allow to connect
//   res.setHeader('Access-Control-Allow-Origin', '*');
//
//   // Request methods you wish to allow
//   res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
//
//   // Request headers you wish to allow
//   res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
//
//   // Set to true if you need the website to include cookies in the requests sent
//   // to the API (e.g. in case you use sessions)
//   res.setHeader('Access-Control-Allow-Credentials', true);
//
//   // Pass to next layer of middleware
//   next();
// });

//config all routes
// app.use('/api/v1/admin', require('./routes/admin'));
app.use('/api/v1/products', require('./routes/products'));
// app.use('/api/v1/public', require('./routes/public'));


//serve static files
app.use(express.static(path.join(__dirname, '/upload/')));


//default error page
app.use((req, res, next) => {
        res.status(404).json({"res": "Sorry can't find that!"});
      });

//listening for port
app.listen(PORT, () => console.log(`Server running on port: ${PORT}`));
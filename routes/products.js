const express = require('express');
const bodyParser = require('body-parser');
const productController = require('../controllers/product/productsController.js');
const materialController = require('../controllers/material/materialController.js');
const validation = require('../config/validation');


const router = express.Router();
// parse application/json
const body = express().use(bodyParser.json());

router.get('/', productController.getProducts);
router.post('/add-products-collection', body, productController.addProductsCollection);

// materials ......................
router.get('/materials', materialController.getMaterials);
router.post('/add-materials-collection', body, materialController.addMaterialsCollection);

module.exports = router;    
const firebase = require('firebase-admin');

// const fs = firebase.initializeApp({
//     apiKey: "AIzaSyCKU13k-bd-amZUxvNvWdyZUx7CGOb8RrY",
//     authDomain: "furniin-d393f.firebaseapp.com",
//     databaseURL: "https://furniin-d393f.firebaseio.com",
//     projectId: "furniin-d393f",
//     storageBucket: "furniin-d393f.appspot.com",
//     messagingSenderId: "629139033631",
//     appId: "1:629139033631:web:e326ec2b46a6ccc92c4f2a",
//     measurementId: "G-RZXM40DEKC",
// });

const serviceAccount = require('../admin.json');
const fs = firebase.initializeApp({
credential: firebase.credential.cert(serviceAccount),
databaseURL: "https://furniin-d393f.firebaseio.com",
authDomain: "furniin-d393f.firebaseapp.com",
});

module.exports = {fs}
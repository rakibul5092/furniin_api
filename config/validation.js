const {check, body, validationResult} = require('express-validator');

//checking for validation errors 
const validate = (req, res, next) => {

    const errors = validationResult(req);
    if (errors.isEmpty()) {
        return next()
    }

    if(req.file !== undefined){
        fs.unlinkSync(req.file.path)
    }
    
    return res.status(200).json({errors: errors.array()});
}

const addProducts = (req, res, next) => {
    return [
        body('products.*.title').trim().not().isEmpty().withMessage('Product title must be somthing.'),
        body('products.*.description').trim().not().isEmpty().withMessage('Product description must be somthing.'),
        body('products.*.main_images').not().isEmpty().withMessage('Product main images must be somthing.'),
        body('products.*.category').not().isEmpty().withMessage('Product category must be somthing.'),
        body('products.*.delivery_types').not().isEmpty().withMessage('Product delivery type must be somthing.'),
        body('products.*.good').custom(value => {
           if( Array.isArray(value)){
            //    console.log("value: ", value[0].price);
             return value.forEach(val => {
                 console.log("price: ", val.price);
                 if(val.price == undefined){return Promise.reject("Undefined Price")}});
           }else{
               console.log("value else: ", value.price);
            return [
                body('products.*.good.code').not().isEmpty().withMessage('must enter code.'),
                body('products.*.good.images').not().isEmpty().withMessage('must give images.'),
                body('products.*.good.inStock').not().isEmpty().withMessage('must give stock.'),

            ]
           }
        }),
        body('products.*.timestamp').not().isEmpty().withMessage('Product timestamp must be somthing.'),
    ];
}


module.exports = {validate, addProducts}
const check = (req, res, next) => {
    const api_key = req.headers('x-api-key');
    const host = req.headers.origin;
    const max = process.env.MAX_REQUEST || 25;         //get the value from env...

    //find the account with host and api key
    let account = users.find(user => user.api_key == api_key && user.host == host);
    if(account){
        let today = new Date().toISOString.split('T')[0];
        let usageIndex = account.usage.findIndex(day => day.date == today);

        if(usageIndex >= 0){
            if(account.usage[usageIndex].count >= max){
                res.status(429).json({error: true, msg: "Max api call exceeded."});
            }else{
                account.usage[usageIndex].count++;
                next();
            }
        }else{
            account.usage.push({ date: today, count: 1 });
            next();
        }
    }else{
        res.status(403).json({ error: { code: 403, message: 'You not allowed.' } });
    }
}




module.exports = {check}
const admin = require("firebase-admin");
const serviceAccount = require("../../firebase.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://furniin-d393f.firebaseio.com",
  authDomain: "furniin-d393f.firebaseapp.com",
});

const getMaterials = async (req, res) => {
  return res.status(200).json({
    error: false,
    res: "Hello! Please contact with our company for further inquiries.",
  });
};

const addMaterialsCollection = async (req, res) => {
  const materials = req.body.materials;
  const api_key = req.body.api_key;
  const baseUrl = req.body.baseUrl;
  let isAuthenticated = false;
  const validationError = [];
  if (!api_key) {
    return res.status(401).json({ err: true, status: "Unauthenticated" });
  } else if (api_key != "") {
    if (!baseUrl || baseUrl == undefined || baseUrl == "") {
      return res
        .status(400)
        .json({ err: true, status: "Base url not provided!" });
    }
    const db = admin.firestore();
    const ref = await db.collection("users");
    ref
      .get()
      .then((result) => {
        result.forEach((item) => {
          if (api_key == item.id) {
            return (isAuthenticated = true);
          }
        });
      })
      .then(() => {
        if (isAuthenticated) {
          if (materials.length > 0) {
            materials.forEach((material, i) => {
              let time = admin.firestore.Timestamp.now();

              material.timestamp = time;
              if (
                material.name == undefined ||
                !material.name ||
                material.name.trim() == ""
              ) {
                validationError.push(`material no ${i + 1} is missing name`);
              }

              if (
                material.images == undefined ||
                !material.images ||
                material.images == ""
              ) {
                validationError.push(`material no ${i + 1} is missing image`);
              }
              if (
                material.code == undefined ||
                !material.code ||
                material.code == ""
              ) {
                validationError.push(`material no ${i + 1} is missing code`);
              }
              if (
                material.category == undefined ||
                !material.category ||
                material.category == null ||
                material.category == ""
              ) {
                validationError.push(
                  `material no ${i + 1} is missing category`
                );
              } else {
                material.category = {
                  id: material.category,
                  data: { category: material.category, timestamp: time },
                };
              }
              if (material.sub_category && material.sub_category != "") {
                material.sub_category = {
                  id: material.sub_category,
                  data: {
                    category_id: material.category.id,
                    sub_category: material.sub_category,
                    timestamp: time,
                  },
                };
              }

              material.timestamp = time;
              material.base_url = baseUrl;
            });

            if (validationError.length > 0) {
              return res.status(400).json({ validationError });
            } else {
              const db = admin.firestore();
              materials.forEach(async (material, i) => {
                await db
                  .collection("imported_materials")
                  .doc(api_key)
                  .collection("imported_materials")
                  .doc(material.code)
                  .set(material)
                  .then(() => {
                    console.log("material inserted");
                  })
                  .catch((err) => {
                    return res.status(400).json({
                      err: true,
                      status: `!!Could not insert material no ${
                        index + 1
                      } please check your data carefully!`,
                    });
                  });
              });
              return res
                .status(200)
                .json({ err: false, status: "done", materials: materials });
            }
          }
        } else {
          return res.status(401).json({ err: true, status: "Unauthenticated" });
        }
      })
      .catch((err) => console.log(err));
  }
};

module.exports = { getMaterials, addMaterialsCollection };

const admin = require("firebase-admin");
const serviceAccount = require("../../firebase.json");

admin.initializeApp(
  {
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://furniin-d393f.firebaseio.com",
    authDomain: "furniin-d393f.firebaseapp.com",
  },
  "products"
);

let time = admin.firestore.Timestamp.now();

const getProducts = (req, res) => {
  return res.status(200).json({
    error: false,
    res: "Hello! Please contact with our company for further inquiries. ",
  });
};

const addProductsCollection = async (req, res) => {
  const products = req.body.products;
  const api_key = req.body.api_key;
  const baseUrl = req.body.baseUrl;
  let isAuthenticated = false;
  const validationError = [];

  if (!api_key) {
    return res.status(401).json({ err: true, status: "Unauthenticated" });
  } else if (api_key != "") {
    if (!baseUrl || baseUrl == undefined || baseUrl == "") {
      return res
        .status(400)
        .json({ err: true, status: "Base url not provided!" });
    }
    const db = admin.firestore();
    const ref = db.collection("users");
    ref
      .get()
      .then((result) => {
        result.forEach((item) => {
          if (api_key == item.id) {
            return (isAuthenticated = true);
          }
        });
      })
      .then(() => {
        if (isAuthenticated) {
          if (products.length > 0) {
            matCollection = [];
            matCat = [];
            matSubCat = [];
            products.forEach((product, i) => {
              if (
                product.product_id == undefined ||
                !product.product_id ||
                product.product_id.trim() == ""
              ) {
                validationError.push(
                  `product no ${i + 1} is missing product_id`
                );
              }
              if (
                product.title == undefined ||
                !product.title ||
                product.title.trim() == ""
              ) {
                validationError.push(`product no ${i + 1} is missing title`);
              }
              if (
                product.description == undefined ||
                !product.description ||
                product.description.trim() == ""
              ) {
                validationError.push(
                  `product no ${i + 1} is missing description`
                );
              }
              if (
                product.main_images == undefined ||
                !product.main_images ||
                product.main_images.length < 1
              ) {
                validationError.push(
                  `product no ${i + 1} is missing main images`
                );
              }
              if (!product.aditional_images) {
                product.aditional_images = [];
              }
              if (
                product.category == undefined ||
                !product.category ||
                product.category == null
              ) {
                validationError.push(`product no ${i + 1} is missing category`);
              } else {
                if (
                  product.category == undefined ||
                  !product.category ||
                  product.category == ""
                ) {
                  validationError.push(
                    `invalid structure of category of product no ${
                      i + 1
                    } in your array`
                  );
                } else {
                  product.category = {
                    id: product.category,
                    data: { category: product.category, timestamp: time },
                  };
                  product.timestamp = admin.firestore.Timestamp.now();
                  product.commision = "";
                  product.base_url = baseUrl;
                  if (
                    product.sub_category &&
                    product.sub_category != undefined &&
                    product.sub_category != "" &&
                    product.sub_category != null
                  ) {
                    product.sub_category = {
                      id: product.sub_category,
                      data: {
                        category_id: product.category.id,
                        sub_category: product.sub_category,
                        timestamp: time,
                      },
                    };
                    if (
                      product.inner_category &&
                      product.inner_category != undefined &&
                      product.inner_category != "" &&
                      product.inner_category != null
                    ) {
                      product.inner_category = {
                        id: product.inner_category,
                        data: {
                          sub_category_id: product.sub_category.id,
                          inner_category: product.inner_category,
                          timestamp: time,
                        },
                      };
                    } else {
                      product.inner_category = "";
                    }
                  } else {
                    product.sub_category = "";
                    product.inner_category = "";
                  }
                }
              }

              if (Array.isArray(product.measures)) {
                product.measures.forEach((measure, m) => {
                  if (
                    measure.name == "" ||
                    !measure.name ||
                    measure.name == undefined ||
                    measure.name == null ||
                    measure.measure == "" ||
                    !measure.measure ||
                    measure.measure == undefined ||
                    measure.measure == null
                  ) {
                    validationError.push(
                      `invalid structure of measures no ${
                        m + 1
                      } of product no ${i + 1} in your array`
                    );
                  }
                });
              }

              if (Array.isArray(product.delivery_types)) {
                product.delivery_types.forEach((d, dI) => {
                  if (
                    d.option == "" ||
                    !d.option ||
                    d.option == undefined ||
                    d.price == "" ||
                    !d.price ||
                    d.price == undefined
                  ) {
                    validationError.push(
                      `invalid structure of delivery type no ${
                        dI + 1
                      } of product no ${i + 1} in your array`
                    );
                  }
                });
              } else {
                validationError.push(
                  `invalid structure of delivery type of product no ${
                    i + 1
                  } in your array`
                );
              }

              if (Array.isArray(product.good)) {
                product.good.forEach((pd, pdi) => {
                  if (
                    pd.price == "" ||
                    pd.price == undefined ||
                    !pd.price ||
                    pd.inStock == "" ||
                    pd.inStock == undefined ||
                    !pd.inStock ||
                    pd.images == null ||
                    pd.images == undefined ||
                    !pd.images ||
                    pd.images.length < 1 ||
                    pd.material == "" ||
                    pd.material == undefined ||
                    !pd.material ||
                    pd.unit == "" ||
                    pd.unit == undefined ||
                    !pd.unit ||
                    pd.code == "" ||
                    pd.code == undefined ||
                    !pd.code ||
                    pd.category == "" ||
                    pd.category == undefined ||
                    !pd.category
                  ) {
                    validationError.push(
                      `good no ${
                        pdi + 1
                      } is missing some property of product no ${
                        i + 1
                      } in your array`
                    );
                  }
                });
              } else if (!Array.isArray(product.good)) {
                if (
                  product.good.price == "" ||
                  product.good.price == undefined ||
                  !product.good.price ||
                  product.good.inStock == "" ||
                  product.good.inStock == undefined ||
                  !product.good.inStock ||
                  product.good.unit == "" ||
                  product.good.unit == undefined ||
                  !product.good.unit
                ) {
                  validationError.push(
                    `good is missing some property of product no ${
                      i + 1
                    } in your array`
                  );
                } else {
                  product.good = {
                    price: product.good.price,
                    unit: product.good.unit,
                    quantity: product.good.quantity || "",
                    inStock: product.good.inStock,
                    expectedStock: product.good.expectedStock || "",
                    colors: product.good.colors || [],
                  };
                }
              } else {
                validationError.push(
                  `good is missing of product no ${i + 1} in your array`
                );
              }
            });

            if (validationError.length > 0) {
              return res.status(400).json({ validationError });
            } else {
              products.forEach(async (product, index) => {
                await db
                  .collection("imported_products")
                  .doc(api_key)
                  .collection("imported_products")
                  .doc(product.product_id)
                  .set(product)
                  .then(() => {
                    console.log("product inserted");
                  })
                  .catch((err) => {
                    return res.status(400).json({
                      err: true,
                      status: `!!Could not insert product no ${
                        index + 1
                      } please check your data carefully!`,
                    });
                  });
              });
              return res
                .status(200)
                .json({ err: false, status: "done", products: products });
            }
          }
        } else {
          return res.status(401).json({ err: true, status: "Unauthenticated" });
        }
      })
      .catch((err) => console.log(err));
  }
};

module.exports = { getProducts, addProductsCollection };
